# ClimApp Tutorial
##Parte I - Esta versão não utiliza a localização do usuário
Esta primeira versão do App iremos consumir a previsão de clima da HGBrasil sem utilizar GPS.

<img src="climapp.jpeg" alt="ScreenShot ClimaApp" height="480px">

### OPCIONAL - Tutorial ClimApp completo com GPS
Neste tutorial voces podem fazer o App completo, utilizem de o preferirem [Link GiLab](https://gitlab.com/esdrasilva/climapphowto). Porem nesta versão não é utilizado RecyclerViews e também não foi utilizado o Jackson para conversão de dados de JSON para POJO.

#### Step I - Dependencias
Vá ao arquivo gradle.build (Module App) e adicione as dependencias abaixo:
```javascript
    implementation 'com.android.volley:volley:1.1.1'
    implementation 'com.fasterxml.jackson.core:jackson-databind:2.11.3'
    implementation "com.fasterxml.jackson.module:jackson-module-kotlin:2.11.3"
```
#### Setp II - Layout
1. Atualize o arquivo `mais_activity.xml` com o código abaixo:
```xml
<?xml version="1.0" encoding="utf-8"?>
<androidx.constraintlayout.widget.ConstraintLayout
        xmlns:android="http://schemas.android.com/apk/res/android"
        xmlns:tools="http://schemas.android.com/tools"
        xmlns:app="http://schemas.android.com/apk/res-auto"
        android:background="@drawable/gradient_animation"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:id="@+id/root_layout"
        tools:context=".MainActivity">
    <TextView
            android:text="Araras"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:id="@+id/textViewCidade"
            android:textColor="@android:color/white"
            android:textSize="32sp"
            app:layout_constraintTop_toTopOf="parent"
            android:layout_marginTop="8dp"
            app:layout_constraintStart_toStartOf="parent"
            android:layout_marginStart="8dp"/>
    <TextView
            android:text='60"'
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:id="@+id/textViewTemperatura"
            android:textSize="120sp"
            android:textColor="@android:color/white"
            app:layout_constraintStart_toStartOf="parent"
            android:layout_marginStart="8dp"
            app:layout_constraintTop_toBottomOf="@+id/textViewData"/>
    <TextView
            android:text="12:57"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:id="@+id/textViewHora"
            android:textSize="24sp"
            android:textColor="@android:color/white"
            app:layout_constraintEnd_toEndOf="parent"
            android:layout_marginEnd="8dp"
            android:layout_marginTop="8dp"
            app:layout_constraintTop_toTopOf="@+id/textViewCidade"
            app:layout_constraintBottom_toBottomOf="@+id/textViewCidade"/>
    <ImageView
            android:layout_width="100dp"
            android:layout_height="100dp"
            android:id="@+id/imageViewIcon"
            app:layout_constraintTop_toTopOf="@+id/linearLayout"
            app:layout_constraintBottom_toBottomOf="@+id/linearLayout"
            app:layout_constraintStart_toEndOf="@+id/linearLayout"
            android:layout_marginStart="24dp"
            app:srcCompat="@drawable/cloud_day" android:backgroundTintMode="src_in"
            app:tint="@android:color/white"
            android:backgroundTint="@android:color/white"/>
    <LinearLayout
            android:orientation="vertical"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:id="@+id/linearLayout"
            app:layout_constraintTop_toTopOf="@+id/textViewTemperatura"
            app:layout_constraintBottom_toBottomOf="@+id/textViewTemperatura"
            android:layout_marginStart="8dp"
            app:layout_constraintStart_toEndOf="@+id/textViewTemperatura">
        <TextView
                android:text="Máx"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:id="@+id/textView3"
                android:textColor="@android:color/white"
                android:layout_weight="1"
                android:textAlignment="viewStart"
        />
        <TextView
                android:text="20"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:id="@+id/textViewMaxima"
                android:textColor="@android:color/white"
                android:layout_weight="1"
                android:textAlignment="viewStart"
                android:textStyle="bold"
                android:textSize="20sp"/>
        <TextView
                android:text="Mín"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:id="@+id/textView8"
                android:textColor="@android:color/white"
                android:layout_weight="1"
                android:textAlignment="viewStart"/>
        <TextView
                android:text="20"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:id="@+id/textViewMinima"
                android:textColor="@android:color/white"
                android:layout_weight="1"
                android:textAlignment="viewStart"
                android:textSize="20sp"
                android:textStyle="bold"/>
    </LinearLayout>
    <TextView
            android:text="Nublado"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:id="@+id/textViewTempoCelula"
            android:textColor="@android:color/white"
            android:textSize="32sp"
            app:layout_constraintTop_toBottomOf="@+id/textViewTemperatura"
            app:layout_constraintStart_toStartOf="parent"
            android:layout_marginStart="8dp"/>
    <TextView
            android:text="05:07"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:id="@+id/textViewNascerDoSol"
            android:textColor="@android:color/white"
            android:layout_weight="1"
            android:textAlignment="viewStart"
            app:layout_constraintStart_toEndOf="@+id/textView11"
            android:layout_marginStart="5dp"
            app:layout_constraintTop_toTopOf="@+id/textView11"
            app:layout_constraintBottom_toBottomOf="@+id/textView11"
            android:textStyle="bold"/>
    <TextView
            android:text="Nascer do Sol: "
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:id="@+id/textView11"
            android:textColor="@android:color/white"
            android:layout_weight="1"
            android:textAlignment="viewStart"
            app:layout_constraintStart_toStartOf="@+id/textViewTempoCelula"
            app:layout_constraintTop_toBottomOf="@+id/textViewTempoCelula"/>
    <TextView
            android:text="17:50"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:id="@+id/textViewPorDoSol"
            android:textColor="@android:color/white"
            android:layout_weight="1"
            android:textAlignment="viewStart"
            app:layout_constraintStart_toEndOf="@+id/textView13"
            android:layout_marginStart="8dp"
            app:layout_constraintTop_toTopOf="@+id/textView13"
            app:layout_constraintBottom_toBottomOf="@+id/textView13"
            android:textStyle="bold"/>
    <TextView
            android:text="Por do Sol: "
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:id="@+id/textView13"
            android:textColor="@android:color/white"
            android:layout_weight="1"
            android:textAlignment="viewStart"
            app:layout_constraintStart_toEndOf="@+id/textViewNascerDoSol"
            android:layout_marginStart="32dp"
            app:layout_constraintTop_toTopOf="@+id/textViewNascerDoSol"
            app:layout_constraintBottom_toBottomOf="@+id/textViewNascerDoSol"/>
    <TextView
            android:text="TextView"
            android:layout_width="226dp"
            android:layout_height="wrap_content"
            android:id="@+id/textViewData"
            android:textColor="@android:color/white"
            android:textAlignment="viewStart"
            app:layout_constraintTop_toBottomOf="@+id/textViewCidade"
            app:layout_constraintStart_toStartOf="@+id/textViewCidade"/>

    <androidx.recyclerview.widget.RecyclerView
            android:id="@+id/recyclerView"
            android:layout_width="match_parent"
            android:layout_height="0dp"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@+id/textView11" />
</androidx.constraintlayout.widget.ConstraintLayout>
```
2. Crie o arquivo `previsao_cell.xml` e adicione o código abaixo:
```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
              xmlns:app="http://schemas.android.com/apk/res-auto" android:orientation="horizontal"
              android:layout_width="match_parent"
              android:layout_height="wrap_content" android:layout_margin="5dp">

    <ImageView
            android:layout_width="75dp"
            android:layout_height="wrap_content"
            app:srcCompat="@drawable/cloudy"
            android:id="@+id/imageViewIconWeather"
            android:layout_weight="1"
            android:cropToPadding="true"
            android:adjustViewBounds="true"
            android:padding="5dp"
            android:layout_margin="3dp"
            app:tint="@android:color/white" />
    <LinearLayout
            android:orientation="vertical"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_weight="2"
            android:layout_margin="3dp">
        <TextView
                android:text="TextView"
                android:layout_width="match_parent"
                android:layout_height="match_parent"
                android:id="@+id/textViewTempoCelula"
                android:textSize="18sp"
                android:textColor="@android:color/white"
                android:layout_marginLeft="3dp"/>
        <TextView
                android:text="TextView"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:id="@+id/textViewMaxMinCelula"
                android:textSize="14sp"
                android:textColor="@android:color/white"
                android:layout_marginLeft="3dp"/>
    </LinearLayout>
</LinearLayout>
``` 
3. Arquivos para animação de Gradiente:
3.1. Crie o arquivo `gradiente_start.xml` no diretório `drawable`.
```xml
<?xml version="1.0" encoding="utf-8"?>
<shape xmlns:android="http://schemas.android.com/apk/res/android"
       android:shape="rectangle">
    <gradient
            android:type="linear"
            android:angle="90"
            android:startColor="@color/colorGradientStart"
            android:centerColor="@color/colorGradientCenter"
            android:endColor="@color/colorGradientEnd"/>
</shape>
```
3.2. Crie o arquivo `gradiente_center.xml` no diretório `drawable`.
```xml
<?xml version="1.0" encoding="utf-8"?>
<shape xmlns:android="http://schemas.android.com/apk/res/android"
       android:shape="rectangle">
    <gradient
            android:type="linear"
            android:angle="90"
            android:startColor="@color/colorGradientCenter"
            android:centerColor="@color/colorGradientEnd"
            android:endColor="@color/colorGradientStart"/>
</shape>
```
3.3. Crie o arquivo `gradiente_end.xml` no diretório `drawable`.
```xml
<?xml version="1.0" encoding="utf-8"?>
<shape xmlns:android="http://schemas.android.com/apk/res/android"
       android:shape="rectangle">
    <gradient
            android:type="linear"
            android:angle="90"
            android:startColor="@color/colorGradientEnd"
            android:centerColor="@color/colorGradientStart"
            android:endColor="@color/colorGradientCenter"/>
</shape>
```
3.3. Crie o arquivo `gradiente_end.xml` no diretório `drawable`.
```xml
<?xml version="1.0" encoding="utf-8"?>
<shape xmlns:android="http://schemas.android.com/apk/res/android"
       android:shape="rectangle">
    <gradient
            android:type="linear"
            android:angle="90"
            android:startColor="@color/colorGradientEnd"
            android:centerColor="@color/colorGradientStart"
            android:endColor="@color/colorGradientCenter"/>
</shape>
```
3.4. Crie o arquivo `gradiente_animation.xml` no diretório `drawable` que ira ser o container dos demais gradientes.
```xml
<?xml version="1.0" encoding="utf-8"?>
<animation-list xmlns:android="http://schemas.android.com/apk/res/android">

    <item android:duration="5000"
          android:drawable="@drawable/gradient_start"/>

    <item android:duration="5000"
          android:drawable="@drawable/gradient_center"/>

    <item android:duration="5000"
          android:drawable="@drawable/gradient_end"/>

</animation-list>
```
3.5. Baixe os icones utilizados e os adicione na pasta `drawable` [Link Icones](https://teams.microsoft.com/_#/school/files/Aula%2016%20-%2005.10.20%20-%20Consumindo%20APIs%20-%20App%20ClimApp?threadId=19%3Aae226e47668242ff96f04a49eeadddfc%40thread.tacv2&ctx=channel&context=Icons&rootfolder=%252Fsites%252FSection_5738.A.-.N.402.104.20192%252FShared%2520Documents%252FAula%252016%2520-%252005.10.20%2520-%2520Consumindo%2520APIs%2520-%2520App%2520ClimApp%252FIcons).

#### Step IV - Codificação
1. Atualize o `MainActivity.java` com o código que realiza a requisição e exibe no LogCat os dados do clima.
```javascript
class MainActivity : AppCompatActivity() {

    val url: String = "https://api.hgbrasil.com/weather?key=a80da7dd&city_name=Artur%20Nogueira,SP"
    lateinit var queue: RequestQueue

    companion object {
        val TAG = this.javaClass.simpleName
    }
    lateinit var adapter: WeatherAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //<editor-fold desc="Gradiente Effect" defaultstate="collapsed">
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
        val animDrawable = root_layout.background as AnimationDrawable
        animDrawable.setEnterFadeDuration(10)
        animDrawable.setExitFadeDuration(5000)
        animDrawable.start()
        //</editor-fold>

        val progressDialog = ProgressDialog(this)
        progressDialog.setTitle("Aguarde")
        progressDialog.setMessage("Obtendo dados...")
        progressDialog.show()

        queue = Volley.newRequestQueue(this)

        // Requisicao web utilizando Volley
        var requisicaoWeb = StringRequest(Request.Method.GET, url, { resultado ->
            progressDialog.dismiss()
            Log.i(TAG, "RESULTADO: [$resultado]")
            // Criacao de objeto Jaclson para conversao de JSON em POJO, foi necessario incluir a dependencia do modulo Kotlin, a dependencia é implementation "com.fasterxml.jackson.module:jackson-module-kotlin:2.11.3"
            val mapper = ObjectMapper().registerKotlinModule()

            // Criar um node para estrair uma ramificação especifica do JSON recebido
            val node: ObjectNode = mapper.readValue(resultado, ObjectNode::class.java)

            // Selecção de ramificação especifica
            val jsonResult = node.get("results").toString()

            // Conversão para POJO
            val weather = mapper.readValue(jsonResult, Weather::class.java)
            Log.d(TAG, weather.toString())

        }, { erro ->
            Log.e(TAG, "ERRO: [${erro.localizedMessage}]")
        })

        queue.add(requisicaoWeb)

    }
}
```
2. Crie as models classe para armazenas os dados da requisição web que lhe retorna os dados em JSON conforme o modelo abaixo:
```json
{
  "by": "city_name",
  "valid_key": true,
  "results": {
    "temp": 23,
    "date": "02/11/2020",
    "time": "13:47",
    "condition_code": "29",
    "description": "Parcialmente nublado",
    "currently": "dia",
    "cid": "",
    "city": "Artur Nogueira, SP",
    "img_id": "29",
    "humidity": 38,
    "wind_speedy": "5.1 km/h",
    "sunrise": "05:23 am",
    "sunset": "06:21 pm",
    "condition_slug": "cloud",
    "city_name": "Artur Nogueira",
    "forecast": [
      {
        "date": "02/11",
        "weekday": "Seg",
        "max": 26,
        "min": 11,
        "description": "Ensolarado com muitas nuvens",
        "condition": "cloudly_day"
      },
      {
        "date": "03/11",
        "weekday": "Ter",
        "max": 26,
        "min": 12,
        "description": "Ensolarado com muitas nuvens",
        "condition": "cloudly_day"
      }
    ]
}

```

2.1. Crie as classes `Weather` e `Forcast`:
```javascript
class Weather {
    var temp: Int = 0
    var date: String = ""
    var time: String = ""
    var condition_code: String = ""
    var description: String = ""
    var currently: String = ""
    var cid: String  = ""
    var city: String  = ""
    var img_id: String = ""
    var humidity: String = ""
    var wind_speedy: String = ""
    var sunrise: String = ""
    var sunset: String = ""
    var condition_slug: String = ""
    var city_name: String = ""
    var forecast: List<Forcast> = Collections.emptyList()
}
```

```javascript
class Forecast {
    var date: String = ""
    var weekday: String = ""
    var max: Int = 0
    var min: Int = 0
    var description: String = ""
    var condition: String  = ""
}
```

2.2. Criação do Adapter para RecyclerView:
```javascript
class WeatherAdapter(private val weather: List<Forecast>): RecyclerView.Adapter<WeatherViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.previsao_cell, parent, false)
        return WeatherViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: WeatherViewHolder, position: Int) {
        val weatherItem = weather[position]
        holder.textViewTime?.text = weatherItem.description
        holder.textViewMinMax?.text = "${weatherItem.weekday} ${weatherItem.date} " +
                "- Máxima: ${weatherItem.max}° - Mínima: ${weatherItem.min}°"
        when(weatherItem.condition){
            "storm" -> holder.image?.setImageResource(R.drawable.storm)
            "snow" -> holder.image?.setImageResource(R.drawable.snow)
            "rain" -> holder.image?.setImageResource(R.drawable.rain)
            "fog" -> holder.image?.setImageResource(R.drawable.fog)
            "clear_day" -> holder.image?.setImageResource(R.drawable.sun)
            "clear_night" -> holder.image?.setImageResource(R.drawable.moon)
            "cloud" -> holder.image?.setImageResource(R.drawable.cloudy)
            "cloudly_day" -> holder.image?.setImageResource(R.drawable.cloud_day)
            "cloudly_night" -> holder.image?.setImageResource(R.drawable.cloudy_night)
        }
    }

    override fun getItemCount(): Int {
        return weather.size
    }
}

class WeatherViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var image: ImageView? = itemView.findViewById(R.id.imageViewIconWeather)
    var textViewTime: TextView? = itemView.findViewById(R.id.textViewTempoCelula)
    var textViewMinMax: TextView? = itemView.findViewById(R.id.textViewMaxMinCelula)
}
``` 

2.3. Crie os métodos que preencherão a Interface Gráfica do App. Na classe `MainActivity.kt` (abaixo do metodo onCreate()), adicione os métodos a seguir:
```javascript
    /**
     * Méto de preenchimento de dados nos widgets com base no objeto Weather
     */
    @SuppressLint("SetTextI18n")
    fun preencherUI(weather: Weather) {
        textViewCidade.text = weather.city_name
        textViewHora.text = weather.time
        textViewData.text = weather.date
        textViewTemperatura.text = "${weather.temp}°"
        textViewMaxima.text = "${weather.forecast[0].max}°"
        textViewMinima.text = "${weather.forecast[0].min}°"
        textViewTempoCelula.text = weather.description
        textViewNascerDoSol.text = weather.sunrise
        textViewPorDoSol.text = weather.sunset
        preencherPrevisoes(weather)
    }

    /**
     * Método de preenchimento da lista de Forcasts (previsoes)
     * Nele cria-se uma isntancia do Adapter e realiza-se a associacao do adapter ao recuclerview
     */
    fun preencherPrevisoes(weather: Weather) {
        adapter = WeatherAdapter(weather.forecast)
        val layoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
    }
``` 

2.4. Confira a classe `MainActivity.kt`.
```javascript
class MainActivity : AppCompatActivity() {

    val url: String = "https://api.hgbrasil.com/weather?key=a80da7dd&city_name=Artur%20Nogueira,SP"
    lateinit var queue: RequestQueue

    companion object {
        val TAG = this.javaClass.simpleName
    }
    lateinit var adapter: WeatherAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //<editor-fold desc="Gradiente Effect" defaultstate="collapsed">
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
        val animDrawable = root_layout.background as AnimationDrawable
        animDrawable.setEnterFadeDuration(10)
        animDrawable.setExitFadeDuration(5000)
        animDrawable.start()
        //</editor-fold>

        val progressDialog = ProgressDialog(this)
        progressDialog.setTitle("Aguarde")
        progressDialog.setMessage("Obtendo dados...")
        progressDialog.show()

        queue = Volley.newRequestQueue(this)

        // Requisicao web utilizando Volley
        var requisicaoWeb = StringRequest(Request.Method.GET, url, { resultado ->
            progressDialog.dismiss()
            Log.i(TAG, "RESULTADO: [$resultado]")
            // Criacao de objeto Jaclson para conversao de JSON em POJO, foi necessario incluir a dependencia do modulo Kotlin, a dependencia é implementation "com.fasterxml.jackson.module:jackson-module-kotlin:2.11.3"
            val mapper = ObjectMapper().registerKotlinModule()

            // Criar um node para estrair uma ramificação especifica do JSON recebido
            val node: ObjectNode = mapper.readValue(resultado, ObjectNode::class.java)

            // Selecção de ramificação especifica
            val jsonResult = node.get("results").toString()

            // Conversão para POJO
            val weather = mapper.readValue(jsonResult, Weather::class.java)
            Log.d(TAG, weather.toString())

            // Metodo de preenchimento de UI a partir do objeto de previsão climatica já convertido a partir do JSON
            preencherUI(weather)
        }, { erro ->
            Log.e(TAG, "ERRO: [${erro.localizedMessage}]")
        })

        queue.add(requisicaoWeb)

    }

    /**
     * Méto de preenchimento de dados nos widgets com base no objeto Weather
     */
    @SuppressLint("SetTextI18n")
    fun preencherUI(weather: Weather) {
        textViewCidade.text = weather.city_name
        textViewHora.text = weather.time
        textViewData.text = weather.date
        textViewTemperatura.text = "${weather.temp}°"
        textViewMaxima.text = "${weather.forecast[0].max}°"
        textViewMinima.text = "${weather.forecast[0].min}°"
        textViewTempoCelula.text = weather.description
        textViewNascerDoSol.text = weather.sunrise
        textViewPorDoSol.text = weather.sunset
        preencherPrevisoes(weather)
    }

    /**
     * Método de preenchimento da lista de Forcasts (previsoes)
     * Nele cria-se uma isntancia do Adapter e realiza-se a associacao do adapter ao recuclerview
     */
    fun preencherPrevisoes(weather: Weather) {
        adapter = WeatherAdapter(weather.forecast)
        val layoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
    }
}
``` 









